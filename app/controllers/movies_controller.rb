class MoviesController < ApplicationController
  def index
    @movies = Movie.order(created_at: :desc).all
  end

  def show
    @movie = Movie.find(params[:id])
    @actors = @movie.actors
  end
end
