class ActorsController < ApplicationController
  def index
    @actors = Actor.order(created_by: :desc).all
  end

  def show
    @actor = Actor.find(params[:id])
    @movies = @actor.movies
  end
end
